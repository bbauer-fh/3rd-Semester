/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

// Task priorities: Higher numbers are higher priority.
#define mainTestTask_Priority ( tskIDLE_PRIORITY + 1 )
//TaskHandle_t xHandle = NULL;
//BaseType_t xReturned;

//void Delay(__IO uint32_t nCount);
//void init(void);
static void Task1( void *pvParameters );
static void Task2( void *pvParameters );
static void Task3( void *pvParameters );
void vTaskDelay( portTickType xTicksToDelay );

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static __IO uint32_t uwTimingDelay;
RCC_ClocksTypeDef RCC_Clocks;
int z=0, s=0;

/* Private function prototypes -----------------------------------------------*/
void Delay(__IO uint32_t nTime);
void Init (void);

/* Private functions ---------------------------------------------------------*/

int main(void)
{
	Init();
 
	xTaskCreate( Task1 , "Task1", configMINIMAL_STACK_SIZE, NULL, 3, NULL );  // LOW
	xTaskCreate( Task2 , "Task2", configMINIMAL_STACK_SIZE, NULL, 2, NULL );  // LOW
	xTaskCreate( Task3 , "Task3", configMINIMAL_STACK_SIZE, NULL, 1, NULL );  // LOW

    vTaskStartScheduler(); // This should never return.
		// Will only get here if there was insufficient memory to create
    // the idle task.
    for( ;; );  
}
//_________________________________________________________
void Task1( void * pvParameters )
{
	// add local variables here
	// no global variables! -> otherwise test failed!
	for(;;)
	{
		LED1on();
		vTaskDelay(500);
		LED1off();
		vTaskDelay(500);
		// USART_SendData(USART2, 'a');
	}
}
//_________________________________________________________
void Task2( void * pvParameters )
{
	for(;;)
	{
		LED2on();
		vTaskDelay(500);
		LED2off();
		vTaskDelay(500);
	}
}
//_________________________________________________________
void Task3( void * pvParameters )
{
	for(;;)
	{
		LED3on();
		vTaskDelay(500);
		LED3off();
		vTaskDelay(500);
	}
}
//_________________________________________________________

//_________________________________________________________

/**
  * @brief  Delay Function.
  * @param  nCount:specifies the Delay time length.
  * @retval None
  */
void Delay(__IO uint32_t nCount)
{
  while(nCount--)
  {
  }
}

void Init (void)
{
	GPIO_InitTypeDef GPIO_IS;
	USART_InitTypeDef USART_IS;

  /* SysTick end of count event each 1ms */
  RCC_GetClocksFreq(&RCC_Clocks);
  SysTick_Config(RCC_Clocks.HCLK_Frequency / 1000);
	NVIC_SetPriority (SysTick_IRQn, 0);
  
  /* Add your application code here */
  /* Insert 50 ms delay */
  Delay(50);
	

  /* Enable the GPIOC peripheral */ 
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
  
  /* Configure MCO1 pin(PA5) in output */
	GPIO_IS.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_IS.GPIO_OType = GPIO_OType_PP;
	GPIO_IS.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3;
	GPIO_IS.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_IS.GPIO_Speed = GPIO_Speed_2MHz;
	
  GPIO_Init(GPIOC, &GPIO_IS);
  
  /* Output HSE clock on MCO1 pin(PA8) ****************************************/ 
  /* Enable the GPIOA peripheral */ 
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  
  /* Configure MCO1 pin(PA5) in output */
	GPIO_IS.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_IS.GPIO_OType = GPIO_OType_PP;
	GPIO_IS.GPIO_Pin = GPIO_Pin_5;
	GPIO_IS.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_IS.GPIO_Speed = GPIO_Speed_2MHz;
	
  GPIO_Init(GPIOA, &GPIO_IS);
	
	
/// ------ Pin fuer USART AF konfigurieren ---------------	
	/* Enable the GPIOA peripheral */ 
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  
  /* Configure MCO1 pin(PA2 & PA3) in alternate function */
	GPIO_IS.GPIO_Mode = GPIO_Mode_AF;
	GPIO_IS.GPIO_OType = GPIO_OType_PP;
	GPIO_IS.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
	GPIO_IS.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_IS.GPIO_Speed = GPIO_Speed_2MHz;
	
  GPIO_Init(GPIOA, &GPIO_IS);
	
	GPIO_PinAFConfig (GPIOA, GPIO_PinSource2, GPIO_AF_USART2);  // Tx AF7 fuer USART2 waehlen
	GPIO_PinAFConfig (GPIOA, GPIO_PinSource3, GPIO_AF_USART2);  // Rx AF7 fuer USART2 waehlen

/// ------ Pin fuer USART AF konfigurieren ---------------	

/// ------ USART konfigurieren ---------------
/* Enable the USART2 peripheral */ 
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);  // 2x APB1 , USART2
	
		USART_IS.USART_BaudRate = 115200;
		USART_IS.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
		USART_IS.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
		USART_IS.USART_Parity = USART_Parity_No;
		USART_IS.USART_StopBits = USART_StopBits_1;
		USART_IS.USART_WordLength = USART_WordLength_8b;
		
		USART_Init (USART2, &USART_IS);
		
		// jede Peripheral muss eingeschaltet werden !!!!!
		USART_Cmd (USART2, ENABLE);

/// ------ USART konfigurieren ---------------	

	
}

///**
//  * @brief  Inserts a delay time.
//  * @param  nTime: specifies the delay time length, in milliseconds.
//  * @retval None
//  */
//void Delay(__IO uint32_t nTime)
//{ 
//  uwTimingDelay = nTime;

//  while(uwTimingDelay != 0);
//}

///**
//  * @brief  Decrements the TimingDelay variable.
//  * @param  None
//  * @retval None
//  */
//void TimingDelay_Decrement(void)
//{
//  if (uwTimingDelay != 0x00)
//  { 
//    uwTimingDelay--;
//  }
//}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
