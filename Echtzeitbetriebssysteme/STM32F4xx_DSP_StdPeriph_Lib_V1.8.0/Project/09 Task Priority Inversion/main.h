
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"


/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void TimingDelay_Decrement(void);
void Delay(__IO uint32_t nTime);

#define LED1on(); GPIO_ResetBits (GPIOC, GPIO_Pin_0);
#define LED1off(); GPIO_SetBits (GPIOC, GPIO_Pin_0);
#define LED1toggle(); GPIO_ToggleBits (GPIOC, GPIO_Pin_0);

#define LED2on(); GPIO_ResetBits (GPIOC, GPIO_Pin_1);
#define LED2off(); GPIO_SetBits (GPIOC, GPIO_Pin_1);
#define LED2toggle(); GPIO_ToggleBits (GPIOC, GPIO_Pin_1);

#define LED3on(); GPIO_ResetBits (GPIOC, GPIO_Pin_2);
#define LED3off(); GPIO_SetBits (GPIOC, GPIO_Pin_2);
#define LED3toggle(); GPIO_ToggleBits (GPIOC, GPIO_Pin_2);

#define LED4on(); GPIO_ResetBits (GPIOC, GPIO_Pin_3);
#define LED4off(); GPIO_SetBits (GPIOC, GPIO_Pin_3);
#define LED4toggle(); GPIO_ToggleBits (GPIOC, GPIO_Pin_3);

#define LED_green_on(); GPIO_SetBits (GPIOA, GPIO_Pin_5);
#define LED_green_off(); GPIO_ResetBits (GPIOA, GPIO_Pin_5);
#define LED_green_toggle(); GPIO_ToggleBits (GPIOA, GPIO_Pin_5);

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
