<?php

    use \Psr\Http\Message\ServerRequestInterface as Request;
    use \Psr\Http\Message\ResponseInterface as Response;

    require __DIR__.'/../vendor/autoload.php';

    $app = new \Slim\App;
    $container = $app->getContainer();

    $container['view'] = function ($container) {
        $view = new \Slim\Views\Twig(__DIR__.'/templates');

        // Instantiate and add Slim specific extension
        $basePath = rtrim(str_ireplace('index.php', '', $container->get('request')->getUri()->getBasePath()), '/');
        $view->addExtension(new Slim\Views\TwigExtension($container->get('router'), $basePath));

        return $view;
    };


    $app->get('/', function ($request, $response, $args) {
        return $this->view->render($response, 'home.html.twig');
    });

    $app->get('/page2', function ($request, $response, $args) {
        return $this->view->render($response, 'page2.html.twig');
    });

    $app->get('/page3', function ($request, $response, $args) {
        return $this->view->render($response, 'page3.html.twig');
    });


    $app->run();
