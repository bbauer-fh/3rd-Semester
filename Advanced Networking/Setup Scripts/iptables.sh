#!/bin/bash

#===========================================================================#
#=====================         DEBUGGING INFO         ======================#

# Watch packets matching iptables rules
# watch -d iptables -L -v -n

# See open connections which pass the iptables rules => IP Tables State Top (top like)
# iptstate

# If ICP forward not working make sure that the following command returns 0!
# sysctl net.ipv4.icmp_echo_ignore_all

#===========================================================================#
#===========================================================================#




#=========================         CONFIG         ==========================#
# IP address + interface configuration
EXT_INTERFACE=ens33 # Interface for traffic out to internet
INTERNAL_SUBNET=10.0.5.0/24
GATEWAY_IP=192.168.10.136
EMAIL_HOST_IP=10.0.5.10
WEB_HOST_IP=10.0.5.100
#===========================================================================#
# Install basic necessary packages and enable forwarding
add-apt-repository universe
apt-get update
apt-get install -y iptables-persistent

sysctl -w net.ipv4.ip.forward=1 # Enable IP forwarding (try without -w if not working)
sysctl -p # List sysctl modifications
echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf # make change persistent



#===========================================================================#
# Empty all rules
## set default policies to let everything in
iptables --policy INPUT ACCEPT
iptables --policy OUTPUT ACCEPT
iptables --policy FORWARD ACCEPT
## start fresh
iptables -Z # zero counters
iptables -t nat -Z
iptables -F # flush (delete) rules
iptables -t nat -F
iptables -X # delete all extra chains

# Block everything by default
iptables -P INPUT DROP
iptables -P FORWARD DROP
iptables -P OUTPUT DROP

# Authorize already established connections
iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

#===========================================================================#
# NAT (Rewrite source IP of outgoing traffic to Gateway IP)
iptables -t nat -A POSTROUTING -s $INTERNAL_SUBNET -j SNAT --to-source $GATEWAY_IP

# Following rule does not work for communication for internal -> gateway -> internaö (only internal -> external nat)
#iptables -t nat -A POSTROUTING ! -d $INTERNAL_SUBNET -o $EXT_INTERFACE -j SNAT --to-source $GATEWAY_IP

#===========================================================================#
# ICMP (Ping)
iptables -A INPUT -p icmp -m state --state NEW -j ACCEPT
iptables -A OUTPUT -p icmp -m state --state NEW -j ACCEPT
iptables -A FORWARD -p icmp -m state --state NEW -j ACCEPT

#===========================================================================#
# SSH
iptables -A INPUT -p tcp --dport 22 -m state --state NEW -j ACCEPT
iptables -A OUTPUT -p tcp --dport 22 -m state --state NEW -j ACCEPT

#===========================================================================#
# DNS
# Allow regular queries (input / output)
iptables -A OUTPUT -p tcp --dport 53 -m state --state NEW -j ACCEPT
iptables -A OUTPUT -p udp --dport 53 -m state --state NEW -j ACCEPT
iptables -A INPUT -p tcp --dport 53 -m state --state NEW -j ACCEPT
iptables -A INPUT -p udp --dport 53 -m state --state NEW -j ACCEPT
iptables -A FORWARD -p tcp --dport 53 -m state --state NEW -j ACCEPT
iptables -A FORWARD -p udp --dport 53 -m state --state NEW -j ACCEPT

#===========================================================================#
# HTTP + HTTPS
# Forward port 80 to Webserver
iptables -t nat -A PREROUTING -p tcp -d $GATEWAY_IP --dport 80 -j DNAT --to-destination $WEB_HOST_IP:80
iptables -A OUTPUT -p tcp --dport 80 -m state --state NEW -j ACCEPT
iptables -A OUTPUT -p tcp --dport 443 -m state --state NEW -j ACCEPT
iptables -A FORWARD -p tcp --dport 80 -m state --state NEW -j ACCEPT
iptables -A FORWARD -p tcp --dport 443 -m state --state NEW -j ACCEPT

#===========================================================================#
# Emails
# SMTP
iptables -t nat -A PREROUTING -p tcp -d $GATEWAY_IP --dport 25 -j DNAT --to-destination $EMAIL_HOST_IP:25
iptables -t nat -A PREROUTING -p tcp -d $GATEWAY_IP --dport 587 -j DNAT --to-destination $EMAIL_HOST_IP:587
iptables -A FORWARD -p tcp --dport 25 -m state --state NEW -j ACCEPT
iptables -A FORWARD -p tcp --dport 587 -m state --state NEW -j ACCEPT
# IMAP
iptables -t nat -A PREROUTING -p tcp -d $GATEWAY_IP --dport 143 -j DNAT --to-destination $EMAIL_HOST_IP:143
iptables -t nat -A PREROUTING -p tcp -d $GATEWAY_IP --dport 993 -j DNAT --to-destination $EMAIL_HOST_IP:993
iptables -A FORWARD -p tcp --dport 143 -m state --state NEW -j ACCEPT
iptables -A FORWARD -p tcp --dport 993 -m state --state NEW -j ACCEPT
# POP3
iptables -t nat -A PREROUTING -p tcp -d $GATEWAY_IP --dport 110 -j DNAT --to-destination $EMAIL_HOST_IP:110
iptables -t nat -A PREROUTING -p tcp -d $GATEWAY_IP --dport 995 -j DNAT --to-destination $EMAIL_HOST_IP:995
iptables -A FORWARD -p tcp --dport 110 -m state --state NEW -j ACCEPT
iptables -A FORWARD -p tcp --dport 995 -m state --state NEW -j ACCEPT

#===========================================================================#
# FTP
iptables -t nat -A PREROUTING -p tcp -d $GATEWAY_IP --dport 20:21 -j DNAT --to-destination $WEB_HOST_IP:20-21
iptables -t nat -A PREROUTING -p tcp -d $GATEWAY_IP --dport 40000:50000 -j DNAT --to-destination $WEB_HOST_IP:40000-50000
iptables -A FORWARD -p tcp --dport 20:21 -d $WEB_HOST_IP -m state --state NEW -j ACCEPT
iptables -A FORWARD -p tcp --dport 40000:50000 -d $WEB_HOST_IP -m state --state NEW -j ACCEPT


# Wireguard
# iptables -A INPUT -p udp --dport 49069 -m state --state NEW -j ACCEPT
# iptables -A OUTPUT -p udp --dport 59042 -m state --state NEW -j ACCEPT
# iptables -A OUTPUT -p udp --dport 38276 -m state --state NEW -j ACCEPT

echo "DONE"