#!/bin/bash

apt-get install -y bind9 bind9utils bind9-doc dnsutils
mkdir -p /etc/bind/zones

# Basic dns setup with allowed hosts and forwarders
cat <<EOT > /etc/bind/named.conf.options
acl "clients" {
	127.0.0.0/8;
	10.0.5.0/24;
};

options {  
	directory "/var/cache/bind";  
	recursion yes;
	allow-recursion { clients; };
	allow-query { clients; };
	allow-transfer { none; };
	listen-on { localhost; 10.0.5.2; };
	forwarders {
		1.1.1.1;
		8.8.4.4;
	};
};
EOT

# Configure available zones
cat <<EOT > /etc/bind/named.conf.local
# Internal zone definitions
zone "bbauer.ant.nwlab" {
	type slave;
	file "data/db.hl.local";
	masters { 10.0.5.1; };
	allow-notify { 10.0.5.1; };
};

zone "5.0.10.in-addr.arpa" {
	type slave;
	file "data/db.5.0.10";
	masters { 10.0.5.1; };
	allow-notify { 10.0.5.1; };
};
EOT

# Finish by restarting bind and execute dig
named-checkconf /etc/bind/named.conf
service bind9 restart
dig @10.0.5.2 ns bbauer.ant.nwlab

echo "DONE"