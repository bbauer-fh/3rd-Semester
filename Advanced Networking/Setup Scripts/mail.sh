#!/bin/bash

# Set hostname to mail to be able to have mail.domain.tld as email server
hostname mail

# The installer asks for the purpose of the installation → Choose "Internet Site"
# As "system mail name" enter the FQDN (in this case `mail.bbauer.ant.nwlab`).
apt-get update
apt-get install -y postfix dovecot-imapd

# Manually check if mydestination is set to correct host!!!
cat /etc/postfix/main.cf

# Restart postfix
service postfix restart

# Add users
echo "Adding user alice"
useradd alice
passwd alice
echo "Adding user bob"
useradd bob
passwd bob

# Create their home and email directories
mkdir -p /home/alice/mail && chown -R alice: /home/alice
mkdir -p /home/bob/mail && chown -R bob: /home/bob

echo "DONE"